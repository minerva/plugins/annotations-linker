require('../css/styles.css');

const $ = require('jquery');
const cookies = require('./cookies');

const pluginName = 'annotations-linker';
const pluginLabel = 'Annotations linker';
const pluginVersion = '1.0.0';
const cookieKey = 'minerva-plugin-annotations-linker-address'



const globals = {
    selected: []
};

// ******************************************************************************
// ********************* PLUGIN REGISTRATION WITH MINERVA *********************
// ******************************************************************************

let minervaProxy;
let pluginContainer;
let pluginContainerId;

const register = function(_minerva) {

    // console.log('registering ' + pluginName + ' plugin');

    $(".tab-content").css('position', 'relative');

    minervaProxy = _minerva;
    pluginContainer = $(minervaProxy.element);

    pluginContainerId = pluginContainer.attr('id');
    if (!pluginContainerId) {
        //the structure of plugin was changed at some point and additional div was added which is the container but does not have any properties (id or height)
        pluginContainer.css('height', '100%');
        pluginContainerId = pluginContainer.parent().attr('id');
    }


    // console.log('minerva object ', minervaProxy);
    // console.log('project id: ', minervaProxy.project.data.getProjectId());
    // console.log('model id: ', minervaProxy.project.data.getModels()[0].modelId);

    initPlugin();
};

const unregister = function () {
    // console.log('unregistering ' + pluginName + ' plugin');

    unregisterListeners();
    return deHighlightAll();
};

const getName = function() {
    return pluginLabel;
};

const getVersion = function() {
    return pluginVersion;
};

const notifyError = function(data) {
    console.error(data);

};

/**
 * Function provided by Minerva to register the plugin
 */
minervaDefine(function (){
    return {
        register: register,
        unregister: unregister,
        getName: getName,
        getVersion: getVersion
        ,notifyError: notifyError
        ,minWidth: 300
        ,defaultWidth: 600
    }
});

function initPlugin () {
    const container = initMainContainer();
    initMainPageStructure(container);
    registerListeners();
}

function registerListeners(){
    minervaProxy.project.map.addListener({
        dbOverlayName: "search",
        type: "onSearch",
        callback: searchListener
    });
}

function unregisterListeners() {
    minervaProxy.project.map.removeAllListeners();
}

// ****************************************************************************
// ********************* MINERVA INTERACTION*********************
// ****************************************************************************


function deHighlightAll(){
    return minervaProxy.project.map.getHighlightedBioEntities().then( highlighted => minervaProxy.project.map.hideBioEntity(highlighted) );
}

// ****************************************************************************
// ********************* PLUGIN STRUCTURE AND INTERACTION*********************
// ****************************************************************************

function getContainerClass() {
    return pluginName + '-container';
}

function initMainContainer(){
    const container = $(`<div class="${getContainerClass()}"></div>`).appendTo(pluginContainer);
    return container;
}

function initMainPageStructure(container){

    container.append(`
        <div class="panel panel-default al-settings">
            <div class="panel-heading" data-toggle="collapse" data-target="#al_panel_${pluginContainerId}">
                Address
                <span class="pull-right glyphicon glyphicon-chevron-down">
            </div>
            <div id="al_panel_${pluginContainerId}" class="panel-body panel-default panel-collapse collapse">
                <input type="text" title="Address with {} as a placeholder" class="form-control al_link" name="msg" 
                placeholder="https://domain/{}" value="${cookies.getCookie(cookieKey)}">
                
            </div>                 
        </div>        
        
        <div class="panel panel-default">
            <div class="panel-body al_annotations">
                <div class="row">
                    <div class="col-md-4 al_annotations_label">Selected species: </div>
                    <div class="col-md-8 al_selected_species"></div>
                </div>
                <div class="row">
                    <div class="col-md-4 al_annotations_label">Annotations: </div>
                    <div class="col-md-8 al_selected_species_annotations"></div>
                </div>                
            </div>
        </div>
    `);

    container.keyup(function (e){
        cookies.setCookie(cookieKey, $(e.target).val());
    })
}

function cleanAnnotations(){
    pluginContainer.find('.al_selected_species').empty();
    pluginContainer.find('.al_selected_species_annotations').empty();
    pluginContainer.find('.al_warning').remove();

}

const unique = function(value, index, self){
    for (let i = 0; i < self.length; i++){
        if (self[i][0] === value[0] && self[i][1] === value[1]) {
            return i === index;
        }
    }
};

const sortPair = function (p1, p2) {
    if (p1[0] == p2[0]) {
        if (p1[1] < p2[1]) return -1;
        else if  (p1[1] === p2[1]) return 0;
        else return 1;
    } else {
        if (p1[0] < p2[0]) return -1;
        else if  (p1[0] === p2[0]) return 0;
        else return 1;
    }
};


function getAddress() {
    return pluginContainer.find('.al_link').val();
}

function setAddress(address) {
    pluginContainer.find('.al_link').val(address);

}

function searchListener(entites){

    // console.log(globals.selected);
    if (entites[0].length > 0) {
        globals.selected = entites[0];
        cleanAnnotations();

        const e = globals.selected[0];
        const name = 'getName' in e ? e.getName() : e.getReactionId();
        pluginContainer.find('.al_selected_species').text(name);


        const annotContainer = pluginContainer.find('.al_selected_species_annotations');
        const link = getAddress();


        e.getReferences()
            .map(annot=>[annot.getType(), annot.getResource()])
            .filter(unique)
            .sort(sortPair)
            .forEach(ann => {


                let annLabel = ann[0];
                let annValue = ann[1];
                if (link.trim() !== "") {
                    let address = link.replace(/\{\}/g, ann[1]);
                    annValue = `<a href="${address}" target="_blank">${ann[1]}</a>`;
                }

                annotContainer.append(`<div class="row">
                    <div class="col-md-5 al_annotation_label">${annLabel}:</div>
                    <div class="col-md-7 al_annotation_value">${annValue}</div>
                </div>`);

            });

        if (link.trim() === "") {
            pluginContainer.find(".al_annotations").append(`<div class="alert alert-danger al_warning" role="alert">
                    Please specify the address and select the species again.
                </div>`);
            return;
        }

    }
}


