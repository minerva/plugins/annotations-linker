# Annotations linker

`Annotations linker` is a plugin which allows the users to open html links 
containing placeholders to be filled with a text string comming from a 
selected annotation. 
 
 ![Plugin example](img/printscreen.png)
 


### General instructions

In order to use the precompiled and publicly available version of the plugin, 
open the plugin menu in the MINERVA's upper left corner (see image below) and click plugins.
In the dialog which appears enter the following address in the URL box: 
`https://minerva-dev.lcsb.uni.lu/plugins/annotations-linker/plugin.js` .
The plugin shows up in the plugins panel on the right hand side of the screen.

### Plugin functionality

The plugin listens to search and click events of the map. When an element is chosen, the plugin prints
the element's name together with all its annotations. If the address is set, annotations IDs are converted
to hyperlinks on the basis of the entered address; each occurrence of curly braces {} 
in the address is replaced by the respective annotation ID. 

The address is stored in the browser's cookies and thus needs to be entered only ones or after 
the cookies are cleaned.  

